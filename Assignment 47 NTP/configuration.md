```
## Last changed: 2020-05-03 15:42:33 CEST
version 12.1X47-D15.4;
system {
    host-name NTPDNSROUTER;
    time-zone Europe/Copenhagen;
    authentication-order [ password radius ];
    root-authentication {
        encrypted-password "$1$wv6diIml$3zjDl5IYiEZgYfeypPJvd/"; ## SECRET-DATA
    }
    radius-server {
        192.168.12.4 {
            port 1812;
            secret "$9$vMPWxdaZjqPT-VYoZG.mz36/u1"; ## SECRET-DATA
        }
    }
    login {
        user peter {
            full-name "peter dahl";
            uid 2000;
            class super-user;
        }
    }
    services {
        ssh;
        dhcp-local-server {
            group serverlan {
                interface ge-0/0/1.0;
            }
        }
        web-management {
            http {
                interface ge-0/0/0.0;
            }
        }
    }
    syslog {
        user * {
            any emergency;
        }
        file messages {
            any any;
            authorization info;
        }
        file interactive-commands {
            interactive-commands any;
        }
    }
    license {
        autoupdate {
            url https://ae1.juniper.net/junos/key_retrieval;
        }
    }
    ntp {
        boot-server 192.168.12.8;
        server 192.168.12.8 version 4;
    }
}
interfaces {
    ge-0/0/0 {
        unit 0;
    }
    ge-0/0/1 {
        unit 0 {
            family inet {
                address 192.168.12.1/24;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                address 192.168.13.1/24;
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                address 10.56.16.87/22;
            }
        }
    }
    ge-0/0/4 {
        unit 0 {
            family inet {
                address 192.168.14.1/24;
            }
        }
    }
}
routing-options {
    static {
        route 192.168.13.0/24 next-hop 192.168.12.1;
        route 192.168.12.0/24 next-hop 192.168.13.1;
        route 0.0.0.0/0 next-hop 10.56.16.1;
    }
}
security {
    screen {
        ids-option untrust-screen {
            icmp {
                ping-death;
            }
            ip {
                source-route-option;
                tear-drop;
            }
            tcp {
                syn-flood {
                    alarm-threshold 1024;
                    attack-threshold 200;
                    source-threshold 1024;
                    destination-threshold 2048;
                    queue-size 2000; ## Warning: 'queue-size' is deprecated
                    timeout 20;
                }
                land;
            }
        }
    }
    nat {
        source {
            rule-set rule_set_serverlan {
                from zone serverlan;
                to zone untrust;
                rule rule-any-to-any {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
            rule-set userlan1 {
                from zone userlan;
                to zone untrust;
                rule s1 {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
            rule-set dmz1 {
                from zone DMZ;
                to zone untrust;
                rule d1 {
                    match {
                        source-address 0.0.0.0/0;
                        destination-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone serverlan to-zone DMZ {
            policy serverlan_to_DMZ_permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone serverlan to-zone userlan {
            policy serverlan_to_DMZ_permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone serverlan {
            policy DMZ_to_serverlan_permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone userlan to-zone DMZ {
            policy userlan_to_DMZ_permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone userlan to-zone serverlan {
            policy userlan_to_serverlan_permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone userlan {
            policy DMZ_to_userlan_permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone DMZ to-zone untrust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone serverlan to-zone untrust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone userlan to-zone untrust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone DMZ {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone userlan {
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
            policy untrust_to_serverlan_permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone serverlan {
            tcp-rst;
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            screen untrust-screen;
            interfaces {
                ge-0/0/0.0 {
                    host-inbound-traffic {
                        system-services {
                            http;
                            https;
                            ssh;
                            telnet;
                            dhcp;
                        }
                    }
                }
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
        security-zone DMZ {
            interfaces {
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
        security-zone userlan {
            interfaces {
                ge-0/0/4.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                    }
                }
            }
        }
    }
}
access {
    address-assignment {
        pool serverlan {
            family inet {
                network 192.168.12.0/24;
                range USERS {
                    low 192.168.12.5;
                    high 192.168.12.10;
                }
                dhcp-attributes {
                    maximum-lease-time 3600;
                    name-server {
                        192.168.12.4;
                    }
                    router {
                        192.168.12.1;
                    }
                }
                host DNS-Server {
                    hardware-address 00:0c:29:42:dd:9b;
                    ip-address 192.168.12.4;
                }
            }
        }
    }
}
```