```
## Last changed: 2020-03-29 12:55:46 UTC
version 12.1X47-D15.4;
system {
    root-authentication {
        encrypted-password "$1$IFeTKfp3$Kdwrg66RsIo0e7XmrIyT/1";
    }
    name-server {
        192.168.13.5;
    }
    services {
        ssh;
        dhcp-local-server {
            group idk {
                interface ge-0/0/2.0;
            }
        }
        web-management {
            http {
                interface ge-0/0/0.0;
            }
        }
    }
    syslog {
        user * {
            any emergency;
        }
        file messages {
            any any;
            authorization info;
        }
        file interactive-commands {
            interactive-commands any;
        }
    }
    license {
        autoupdate {
            url https://ae1.juniper.net/junos/key_retrieval;
        }
    }
}
interfaces {
    ge-0/0/1 {
        unit 0 {
            family inet {
                address 192.168.12.1/24;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                address 192.168.13.1/24;
            }
        }
    }
    ge-0/0/3 {
        unit 0 {
            family inet {
                address 10.56.16.1/22;
            }
        }
    }
}
routing-options {
    static {
        route 192.168.13.0/24 next-hop 10.56.16.1;
    }
}
security {
    screen {
        ids-option untrust-screen {
            icmp {
                ping-death;
            }
            ip {
                source-route-option;
                tear-drop;
            }
            tcp {
                syn-flood {
                    alarm-threshold 1024;
                    attack-threshold 200;
                    source-threshold 1024;
                    destination-threshold 2048;
                    queue-size 2000;
                    timeout 20;
                }
                land;
            }
        }
    }
    nat {
        source {
            rule-set ruleset1 {
                from zone trust;
                to zone untrust;
                rule rule1 {
                    match {
                        source-address 0.0.0.0/0;
                    }
                    then {
                        source-nat {
                            interface;
                        }
                    }
                }
            }
        }
    }
    policies {
        from-zone trust to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
            policy test {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone untrust to-zone trust {
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
    }
    zones {
        security-zone trust {
            tcp-rst;
            host-inbound-traffic {
                system-services {
                    all;
                }
                protocols {
                    all;
                }
            }
            interfaces {
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            http;
                        }
                    }
                }
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                            dhcp;
                            dns;
                            all;
                        }
                    }
                }
            }
        }
        security-zone untrust {
            screen untrust-screen;
            host-inbound-traffic {
                system-services {
                    https;
                }
            }
            interfaces {
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
            }
        }
    }
}
access {
    address-assignment {
        pool pappnetwork {
            family inet {
                network 192.168.13.0/24;
                range r1 {
                    low 192.168.13.7;
                    high 192.168.13.26;
                }
                dhcp-attributes {
                    name-server {
                        192.168.13.5;
                    }
                    propagate-settings ge-0/0/2.0;
                }
            }
        }
    }
}
```